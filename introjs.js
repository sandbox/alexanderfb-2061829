
jQuery(document).ready( function() {
   // Only display the button if at least one intro is present.
   if( 0 < jQuery('.intro-js').length ) {
      jQuery( 
         '<a id="introjs-start-link" href="javascript:void(0);" ' +
         // TODO: Localize button?
         'onclick="javascript:introJs().start();">Help</a>'
      ).appendTo( 'body' ).slideDown();
   }
} );

